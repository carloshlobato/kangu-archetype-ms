import { HttpStatus } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { ICustomLogger } from 'src/infrastructure/logger/protocols/ICustomLogger';

export function RestrictRequestMiddleware(kanguLoggerService: ICustomLogger) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (
      req.method !== 'GET' &&
      req.method !== 'OPTIONS' &&
      req.method !== 'DELETE' &&
      (!req.is('application/json'))
    ) {
      if (!req.is('application/json')) {
        kanguLoggerService.kanguError(
          `recebido uma requisicao diferente de application/json (${req.get(
            'content-type',
          )}), mensagem descartada`,
          req.__trackId__,
        );
        return res
          .status(HttpStatus.BAD_REQUEST)
          .send('permitido apenas application/json');
      }
    } else {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT');
      res.setHeader(
        'Access-Control-Allow-Headers',
        'X-Requested-With,content-type,Authorization',
      );
      res.setHeader('Access-Control-Allow-Credentials', 'true');
      next();
    }
  };
}
