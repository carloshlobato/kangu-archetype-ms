type IConstructor<T> = new (...args: any[]) => T;

export const mapperToEntity = <T>(Entity: IConstructor<T>, value: any): T => {
  const [resultDb] = value.ops;
  const { _id, ...targetValues } = resultDb;
  return new Entity(targetValues);
};
