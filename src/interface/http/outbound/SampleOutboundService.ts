import { HttpStatus, Injectable } from '@nestjs/common';
import { SampleRequestClient } from '@src/infrastructure';
import { ISampleOutboundService } from './protocols/ISampleOutboundService';

@Injectable()
export class SampleOutboundService implements ISampleOutboundService {
    constructor(
        private readonly sampleRequestClient: SampleRequestClient,
    ) { }

    public async getById(id: string): Promise<any> {
        try {
            const { data } = await this.sampleRequestClient.get(id);

            return data;
        } catch (error) {
            if (error.status === HttpStatus.NOT_FOUND) {
                return null;
            }
            throw error;
        }
    }
}