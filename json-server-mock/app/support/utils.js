import uuidv4 from 'uuid/v4.js'
import _ from 'lodash'

export const SPECIAL_CHAR = '&';
export const newUUID = () => uuidv4();

export const getFolderDataByList = folder => name => {
    if (!global.mockData[folder])
        global.mockData[folder] = [];

    if (!name)
        return global.mockData[folder].map(({ data }) => data);

    const result = global.mockData[folder].filter(({ data }) => {
        const queries = name.split(SPECIAL_CHAR).slice(1);

        const find = queries.every(el => {
            const [key, value] = el.split('=');
            return !!(data[key] && data[key] === value)
        });

        return find;
    });
    return result ? result.map(({ data }) => data) : null;
};

export const getFolderDataById = folder => ({ id, param }) => {
    if (!global.mockData[folder])
        global.mockData[folder] = [];

    const result = global.mockData[folder].find(({ data }) => data[param] === id);
    return result ? result.data : null;
};

export const updateFolderData = ({ folder, id, newBody, param }) => {
    if (!global.mockData[folder])
        return null;

    const result = global.mockData[folder].find(({ data }) => data[param] === id);

    if (!result)
        return null;

    const newData = _.merge(result.data, newBody);
    return newData;
};

export const dropFromFolderData = ({ folder, id, param }) => {
    if (!global.mockData[folder])
        return null;

    const resultIndex = global.mockData[folder].findIndex(({ data }) => data[param] === id);

    if (resultIndex === -1)
        return null;

    global.mockData[folder].splice(resultIndex, 1);
    return true;
};

export const saveFolderData = folder => data => {
    if (!global.mockData[folder])
        global.mockData[folder] = [];

    global.mockData[folder].push(data);
};

export const generateQueryParams = query =>
    Object.entries(query).reduce(
        (sum, [name, value]) => `${sum}${SPECIAL_CHAR}${name}=${value}`, ''
    );

export const getFileName = path => path.split('\\').pop().split('/').pop();
