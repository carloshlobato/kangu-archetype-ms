export interface IHttpDispatcher {
  dispatch<T>(options: any): Promise<void>
}
