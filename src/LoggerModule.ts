import { Module } from '@nestjs/common';
import { KanguLoggerService } from './infrastructure/logger/KanguLoggerService';

@Module({
  providers: [KanguLoggerService],
  exports: [KanguLoggerService],
})
export class LoggerModule { }
