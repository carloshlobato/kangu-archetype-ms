import jsonServer from 'json-server'

import { setupMiddlewares } from './support/setupMiddlewares.js';
import { setupRoutes } from './support/setupRoutes.js';

export const start = initDataLoader => {
    initDataLoader();

    const router = jsonServer.router(global.mockData);
    const server = jsonServer.create();

    setupMiddlewares(jsonServer, server);
    setupRoutes(server, router);

    const port = process.env.PORT || 4000;

    server.listen(port, error => {
        if (error) {
            console.error(`Json Server Mock => Failed to start at ${port}`);
            throw error;
        }
        console.log(`Json Server Mock => Listen at ${port}`);
    });
};
