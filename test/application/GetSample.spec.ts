import { Test } from '@nestjs/testing';
import { GetSample } from '@src/application';
import { NotFoundException } from '@nestjs/common';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

describe('application :: GetSample', () => {
    let getSample: GetSample;
    let sampleOutboundService: any;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [GetSample],
            providers: [
                KanguLoggerService,
                {
                    provide: 'SampleOutboundService',
                    useValue: {
                        getById: jest.fn()
                    }
                },
                {
                    provide: 'KanguLoggerService',
                    useValue: {
                        error: jest.fn((data) => console.error(data))
                    }
                },
                {
                    provide: 'SampleRepositoryWriter',
                    useValue: {}
                }
            ],
        }).setLogger(new KanguLoggerService).compile();

        getSample = moduleRef.get<GetSample>(GetSample);
        sampleOutboundService = moduleRef.get<'SampleOutboundService'>('SampleOutboundService');
    });

    describe('invoke()', () => {
        test('execute successfully when data is received and no error occurs', async () => {
            const invokeSpy = jest.spyOn(sampleOutboundService, 'getById').mockResolvedValue({ data: '1' });
            const result = await getSample.invoke('any_value');
            expect(invokeSpy).toHaveBeenCalledWith('any_value');
            expect(result).toEqual({ data: '1' });
        });

        test('throws Not Found Exception when no data is found', async () => {
            const invokeSpy = jest.spyOn(sampleOutboundService, 'getById').mockResolvedValue(null);
            try {
                await getSample.invoke('any_value');
            }
            catch (error) {
                expect(invokeSpy).toHaveBeenCalledWith('any_value');
                expect(error).toBeInstanceOf(NotFoundException);
                expect(error.message).toEqual('Client id any_value not found')
            }
        });
    });
});
