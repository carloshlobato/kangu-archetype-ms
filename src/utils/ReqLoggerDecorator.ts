import { Inject } from '@nestjs/common';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';
import { maskSensitiveInformations } from './Masks';

export function ReqLogger(origin?: string) {
    const injectLogger = Inject(KanguLoggerService);

    return (target: any, propertyKey: string, propertyDescriptor: PropertyDescriptor) => {
        injectLogger(target, 'logger');
        const originalMethod = propertyDescriptor.value;

        propertyDescriptor.value = async function (...args: any[]) {
            const stringParam = args.find(el => typeof el === 'string');
            const objectParam = args.find(el => typeof el === 'object');
            const hasStringParam = stringParam ? `, id ${stringParam}` : '';
            try {
                this.logger.kanguInfo(`REQ BODY ${origin} => ${objectParam ? maskSensitiveInformations(objectParam) : null}${hasStringParam}`);
                const data = await originalMethod.apply(this, args);
                this.logger.kanguInfo(`RES BODY ${origin} => ${maskSensitiveInformations(data || null)}${hasStringParam}`);
                return data;
            } catch (error) {
                this.logger.kanguInfo(`RES BODY ${origin} => ${error.data ? JSON.stringify(error.data) : error.message}${hasStringParam}`);
                throw error;
            }
        };
    };
}
