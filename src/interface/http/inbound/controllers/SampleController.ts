import { ApiBody, ApiProperty, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Inject, Body, Post, HttpCode, Get, Param, Delete, Put, UsePipes, ValidationPipe, Headers } from '@nestjs/common';

import { CreateSampleDTO } from '../dto/CreateSampleDTO';
import { GetSample, DeleteSample } from '../../../../application';
import { SampleEntity } from 'src/domain';
import { ReqLogger } from '@src/utils/ReqLoggerDecorator';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

const API_VERSION = process.env.API_VERSION || 'v1';

@ApiTags('sample')
@Controller(`${API_VERSION}/sample`)
export class SampleController {
    constructor(
        @Inject('GetSample')
        private readonly getSample: GetSample,
        @Inject('DeleteSample')
        private readonly deleteSample: DeleteSample,
        @Inject('KanguLoggerService')
        private readonly kanguLoggerService: KanguLoggerService
    ) { }

    @ApiBody({ type: CreateSampleDTO })
    @Post()
    @HttpCode(204)
    @ReqLogger('Sample MS')
    public async create(@Body() body: CreateSampleDTO): Promise<void> {
        try {
            await this.deleteSample.invoke(body.name);
        } catch (error) {
            this.kanguLoggerService.kanguError(error);
            throw error;
        }
    }

    @ApiProperty()
    @Get(':id')
    @UsePipes(new ValidationPipe({ transform: true }))
    @ReqLogger('Sample MS')
    public async getById(
        @Param('id') id: string): Promise<SampleEntity> {
        try {
            return await this.getSample.invoke(id);
        } catch (error) {
            this.kanguLoggerService.kanguError(error);
            throw error;
        }
    }

    @ApiProperty()
    @Delete(':id')
    @HttpCode(204)
    @ReqLogger('Sample MS')
    public async delete(
        @Param('id') id: string,
    ): Promise<any> {
        try {
            return await this.deleteSample.invoke(id);
        } catch (error) {
            this.kanguLoggerService.kanguError(error);
            throw error;
        }
    }
}
