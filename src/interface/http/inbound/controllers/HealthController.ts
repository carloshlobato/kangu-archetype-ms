import { MongoClient } from 'mongodb';
import { Controller, Get, Inject, InternalServerErrorException } from '@nestjs/common';

import { HealthCheck } from '@nestjs/terminus';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

enum HealthStatus {
  UP = 'up',
  DOWN = 'down',
}

@Controller()
export class HealthController {
  private readonly message: string = 'servico ativo e conectado ao banco de dados';
  private readonly errorMessage: string = 'erro ao executar uma consulta ao banco de dados: ';

  constructor(
    // @Inject('DATABASE_CONNECTION')
    // private readonly db: MongoClient,
    private readonly logger: KanguLoggerService,
  ) { }

  @Get('/healthcheck')
  @HealthCheck()
  public async healthCheck() {
    try {
      const healthCheck = {
        status: HealthStatus.UP,
        message: this.message,
      };

      // if (!this.db.isConnected()) {
      //   throw new Error('lost connection with mongodb');
      // }

      this.logger.kanguInfo(JSON.stringify(healthCheck));
      return healthCheck;
    } catch (error) {
      // await this.db.close();
      this.logger.kanguError(this.errorMessage + error.message);
      throw new InternalServerErrorException(this.errorMessage + error.message);
    }
  }

}
