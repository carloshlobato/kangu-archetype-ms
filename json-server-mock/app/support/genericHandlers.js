import _ from 'lodash';

import {
    generateQueryParams,
    getFolderDataById,
    getFolderDataByList,
    saveFolderData,
    dropFromFolderData,
    updateFolderData,
    newUUID,
} from './utils.js';

export const getById = ({
    folder,
    param,
}) => (req, res) => {
    const id = req.params[param];
    const result = getFolderDataById(folder)({ id, param });

    if (!result)
        return res.status(404).end();

    res.status(200).json(result).end();
};

export const list = ({
    folder,
}) => (req, res) => {
    const result = getFolderDataByList(folder)(
        generateQueryParams(req.query)
    );

    res.status(200).json(result || []).end();
};

export const post = ({
    folder,
    idName,
}) => (req, res) => {
    const id = newUUID();
    const additional_data = {
        [idName]: id
    };
    const data = _.merge(req.body, { additional_data });

    saveFolderData(folder)({
        filename: id,
        data
    });

    res.status(201).json(data).end();
};

export const put = ({
    folder,
    param,
}) => (req, res) => {
    const id = req.params[param];
    const result = updateFolderData({ folder, id, newBody: req.body, param });

    if (!result)
        return res.status(404).end();

    res.status(200).json(result).end();
};

export const drop = ({
    folder,
    param,
}) => (req, res) => {
    const id = req.params[param];
    const result = dropFromFolderData({ folder, id, param });

    if (!result)
        return res.status(404).end();

    res.status(202).end();
};
