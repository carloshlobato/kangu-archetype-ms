import { Inject, Injectable } from '@nestjs/common';

import {
    ISampleRepositoryReader,
    ISampleRepositoryWriter
} from '@domain/index';
import { SampleEntity } from '../entities/SampleEntity';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

@Injectable()
export class SampleService {


    constructor(
        @Inject('SampleRepositoryReader')
        private readonly sampleRepositoryReader: ISampleRepositoryReader,
        @Inject('SampleRepositoryWriter')
        private readonly sampleRepositoryWriter: ISampleRepositoryWriter,

        private readonly logger: KanguLoggerService,
    ) {
    }


    public async getSampleValues(): Promise<any> {

        const result = await this.sampleRepositoryReader.getById("id");

        if (!result) {
            return null;
        }

        return result;

    }

}