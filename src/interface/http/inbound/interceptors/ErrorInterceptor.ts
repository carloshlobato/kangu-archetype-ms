import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';
import { Observable, ObservableInput } from 'rxjs';
import { catchError } from 'rxjs/operators';

const logger = new KanguLoggerService();
@Injectable()
export class ErrorInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(catchError(ErrorInterceptor.processError));
  }

  static processError(error: any): ObservableInput<any> {
    logger.kanguError(error.data);

    throw new HttpException(
      error.status ? ({
        code: error.code,
        message: error.message,
        details: [error.getResponse()],
      }) : ({
        message: 'Internal Server Error',
        details: [{
          message: error.message
        }],
      }),
      error.status ? error.status : HttpStatus.INTERNAL_SERVER_ERROR
    );
  }
}
