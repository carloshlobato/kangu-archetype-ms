
## Getting Started

```
git clone this repo
```

### Structure
```
├── src
│   ├── application
│   │   ├── appName.ts
│   ├── config
│   │   ├── database.ts
│   ├── domain
│   │   ├── enums
│   │   │   └── *Enum.ts
│   │   ├── entities
│   │   │   └── *Entity.ts
│   │   └── service
│   │       └── *Service.ts
│   │   └── protocols
│   │       └── IProtocol.ts
│   │   └── valueObjects
│   │       └── *.ts
│   ├── infrastructure
│   │   ├── database
│   │   │   └── repositories
│   │   │    	└── *.ts
│   │   │   └── db
│   │   │    	└── *.ts
│   │   ├── http
│   │   │   └── clients
│   │   │    	└── *.ts
│   │   ├── logger
│   │   │   └── *.ts
│   │   └── repository
│   │       └── *.repository.ts
│   ├── interface
│   │   ├── database
│   │   │   └── repositories
│   │   │    	└── *.ts
│   │   │   └── db
│   │   │    	└── *.ts
│   │   ├── http
│   │   │   └── inbound
│   │   │    	└── controllers
│   │   │    	└── dto
│   │   │    	└── filters
│   │   │    	└── interceptors
│   │   │    	└── middlewares
│   │   │   └── outbound
│   │   │    	└── protocols
│   │   ├── schedulers
│   │   │   └── *.ts
│   ├── main.ts
│   ├── ApplicationModule.ts
│   ├── DatabaseModule.ts
│   ├── InfrastructureModule.ts
│   ├── InterfaceInboundModule.ts
│   ├── InterfaceOutboundModule.ts
│   ├── LoggerModule.ts
│   ├── SchedulerModule.ts
│   ├── MainModuke.ts
├── jest.config.json
├── localhost.sqlite
├── nest-cli.json
├── nodemon-debug.json
├── nodemon.json
├── package-lock.json
├── package.json
├── README.md
├── tsconfig.build.json
├── tsconfig.json
└── tslint.json

```

### Prerequisites

  * node 10+
  * nestjs 

```bash
$ docker run --name dev-mongo -p 27017:27017 -d mongo

```

### Installing

```bash

$ npm install 

```

If you see this everything all fine 


```

added 898 packages from 578 contributors and audited 876746 packages in 11.087s
found 0 vulnerabilities

```

### Running

```bash

$ npm start:dev

```

### Running with Docker


```bash
$ docker-compose -f docker-compose.yml up -d  

```

## Running the tests

```bash
# unit tests with coverage
$ npm run test:cov

```


## Built With

* [Nest](https://github.com/nestjs/nest)  - The framework used

* [node.js](https://nodejs.org/en/)- Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine

* [jestjs](https://jestjs.io/en/) Jest is a delightful JavaScript Testing Framework with a focus on simplicity



## Authors

*  **Carlos Lobato**

  
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
