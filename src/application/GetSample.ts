import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { ISampleRepositoryWriter, SampleEntity } from '@src/domain';
import { ISampleOutboundService } from '@interface/index';

@Injectable()
export class GetSample {
  constructor(
    @Inject('SampleOutboundService')
    private readonly sampleOutboundService: ISampleOutboundService,
    @Inject('SampleRepositoryWriter')
    private readonly sampleRepositoryWriter: ISampleRepositoryWriter,
  ) { }

  public async invoke(id: string): Promise<SampleEntity> {
    const result = await this.sampleOutboundService.getById(id);

    if (!result) {
      throw new NotFoundException(`Client id ${id} not found`);
    }

    return result;
  }
}