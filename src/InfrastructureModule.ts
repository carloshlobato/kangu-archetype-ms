import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios'
import { DatabaseModule } from './DatabaseModule';
import {
  HttpClient,
  SampleRequestClient
} from '@infrastructure/index';
import { HttpDispatcher } from '@interface/http/outbound/HttpDispatcher';
import { KanguLoggerService } from './infrastructure/logger/KanguLoggerService';

@Module({
  imports: [
    HttpModule,
    DatabaseModule,
  ],
  controllers: [],
  providers: [
    KanguLoggerService,
    HttpClient,
    HttpDispatcher,
    SampleRequestClient
  ],
  exports: [
    DatabaseModule,
    HttpDispatcher,
    KanguLoggerService,
    SampleRequestClient
  ],
})
export class InfrastructureModule { }
