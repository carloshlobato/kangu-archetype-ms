import { LoggerService } from '@nestjs/common';

export interface ICustomLogger extends LoggerService {
  kanguLog: (level: string, message: string, trackId: string) => any;
  kanguError: (message: string, trackId?: string) => any;
  kanguWarn: (message: string, trackId?: string) => any;
  kanguInfo: (message: string, trackId?: string) => any;
  kanguDebug: (message: string, trackId?: string) => any;
}
