import { Module } from '@nestjs/common';
import {
  SampleService,
} from '@domain/index';
import { InfrastructureModule } from './InfrastructureModule';

import { SampleOutboundService } from './interface/http/outbound/SampleOutboundService';


const sampleOutboundService = {
  provide: 'SampleOutboundService',
  useClass: SampleOutboundService
};

const sampleService = {
  provide: 'SampleService',
  useClass: SampleService
}


const providers = [
  sampleService,
  sampleOutboundService,
];

@Module({
  imports: [
    InfrastructureModule
  ],
  providers,
  exports: providers,
})
export class InterfaceOutboundModule { }
