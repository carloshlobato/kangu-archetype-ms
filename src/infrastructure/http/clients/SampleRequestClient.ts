import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { HttpDispatcher } from '@interface/http/outbound/HttpDispatcher';

@Injectable()
export class SampleRequestClient {
    private httpConfig: any;

    constructor(
        private readonly httpDispatcher: HttpDispatcher,
        configService: ConfigService,
    ) {
        this.httpConfig = configService.get('httpClient').sampleServiceMs;
    }

    public async get(value: string): Promise<any> {
        return await this.httpDispatcher.dispatch(
            this.buildRequestConfigs(value)
        );
    }

    private buildRequestConfigs(value?: string) {
        return {
            baseURL: this.httpConfig.url.base,
            url: `${this.httpConfig.url.path}/${value}`,
            method: 'get'
        };
    }
}