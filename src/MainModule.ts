import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import databaseConfig from './config/database';
import httpClient from '@config/httpClient';

import { InterfaceInboundModule } from './InterfaceInboundModule';
import { InfrastructureModule } from './InfrastructureModule';
import { ApplicationModule } from './ApplicationModule';
import { InterfaceOutboundModule } from './InterfaceOutboundModule';
import { SchedulerModule } from './SchedulerModule';

const CONFIG_MODULE_OPTIONS = {
  isGlobal: true,
  load: [
    databaseConfig,
    httpClient,
  ],
};

@Module({
  imports: [
    ConfigModule.forRoot(CONFIG_MODULE_OPTIONS),
    InfrastructureModule,
    InterfaceOutboundModule,
    InterfaceInboundModule,
    ApplicationModule,
    SchedulerModule

  ],
})
export class MainModule { }
