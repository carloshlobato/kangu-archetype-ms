import { ConfigService } from '@nestjs/config';
import { MongoDbProviderConnection } from '@src/infrastructure';
import { MongoClient } from 'mongodb';

describe('infrastructure :: database :: mongodb :: Mongo DB Provider Connection', () => {
    afterAll(() => {
        jest.clearAllMocks();
    });

    test('should call createPool with valid properties', async () => {
        const createPool = jest.spyOn(MongoClient, 'connect').mockImplementationOnce(() => ({
            readPreference: { mode: '' },
            getConnection: jest.fn(),
            on: jest.fn()
        }));

        const config = {
            user: 'admin',
            password: 'root',
            url: 'http',
            database: 'database',
            port: '27017',
            hosts: '["host1","host2","host3"]',
            poolMin: 0,
            poolMax: 4,
            poolIncrement: 1,
        };

        const fakeConfigService = {
            get: jest.fn().mockReturnValue(config),
        } as unknown as ConfigService;

        const connection = new MongoDbProviderConnection(fakeConfigService);

        await connection.connect();

        expect(createPool).toHaveBeenCalledWith("undefined://nullhost1:27017,host2:27017,host3:27017/database?authSource=undefined", { 'authSource': undefined, 'replicaSet': '', 'useNewUrlParser': true, 'useUnifiedTopology': true });
    });
    test('returns connection when connection is already been set and not call mongo connect function', async () => {
        const createPool = jest.spyOn(MongoClient, 'connect').mockImplementationOnce(() => ({
            readPreference: { mode: '' },
            getConnection: jest.fn(),
            on: jest.fn()
        }));

        const config = {
            user: 'admin',
            password: 'root',
            url: 'http',
            database: 'database',
            port: '27017',
            hosts: '["host1","host2","host3"]',
            poolMin: 0,
            poolMax: 4,
            poolIncrement: 1,
        };

        const fakeConfigService = {
            get: jest.fn().mockReturnValue(config),
        } as unknown as ConfigService;

        const connection = new MongoDbProviderConnection(fakeConfigService);

        await connection.connect();
        await connection.connect();

        expect(createPool).toHaveBeenCalledTimes(1)
    });

    test('should throw', async () => {
        jest.spyOn(MongoClient, 'connect').mockImplementationOnce(() => ({
            getConnection: jest.fn(() => {
                throw new Error('Error');
            }),
        }));

        const config = {
            userName: 'admin',
            password: 'root',
            connectString: 'http://db.com',
            poolMin: 0,
            poolMax: 4,
            poolIncrement: 1,
        };

        const fakeConfigService = {
            get: jest.fn().mockReturnValue(config),
        } as unknown as ConfigService;

        const fakeConsole = {
            error: jest.fn()
        }

        jest.spyOn(console, 'error').mockImplementationOnce(() => { return { error: jest.fn() } })

        const connection = new MongoDbProviderConnection(fakeConfigService);

        await expect(() => connection.connect()).rejects.toThrow();
    });
});