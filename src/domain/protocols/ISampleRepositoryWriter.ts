import { SampleEntity } from "../entities/SampleEntity";

export interface ISampleRepositoryWriter {
    delete: (sampleEntity: SampleEntity) => Promise<boolean>;
}
