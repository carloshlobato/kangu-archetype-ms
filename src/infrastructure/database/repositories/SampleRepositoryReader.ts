import { Inject, Injectable } from '@nestjs/common';
import { Collection, MongoClient } from 'mongodb';
import { ISampleRepositoryReader } from '@domain/index';


@Injectable()
export class SampleRepositoryReader implements ISampleRepositoryReader {
    private readonly collection: Collection;

    constructor(
        // @Inject('DATABASE_CONNECTION')
        // private dbConnection: MongoClient,
    ) {
        // this.collection = this.dbConnection.db().collection('sample');
    }

    public async getById(id: string) {
        if (id == 'teste') {
            return { id: 'sample_id', name: 'sample_name' };
        } else {
            return null;
        }

        // return this.collection.findOne({
        //     id
        // });
    }

}

