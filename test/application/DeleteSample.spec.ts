import { Test } from '@nestjs/testing';
import { DeleteSample } from '@src/application';
import { SampleRepositoryReader } from '@src/infrastructure';
import { NotFoundException } from '@nestjs/common';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';


describe('application :: DeleteSample', () => {
    let getSample: DeleteSample;
    let sampleRepositoryReader: any;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [DeleteSample],
            providers: [
                KanguLoggerService,
                {
                    provide: 'SampleRepositoryReader',
                    useValue: {
                        getById: jest.fn()
                    }
                },
                {
                    provide: 'KanguLoggerService',
                    useValue: {
                        error: jest.fn((data) => console.error(data))
                    }
                },
                {
                    provide: 'SampleRepositoryWriter',
                    useValue: {}
                }
            ],
        }).setLogger(new KanguLoggerService).compile();

        getSample = moduleRef.get<DeleteSample>(DeleteSample);
        sampleRepositoryReader = moduleRef.get<'SampleRepositoryReader'>('SampleRepositoryReader');
    });

    describe('invoke()', () => {
        test('execute successfully when data is received and no error occurs', async () => {
            const invokeSpy = jest.spyOn(sampleRepositoryReader, 'getById').mockResolvedValue({ data: '1' });
            const result = await getSample.invoke('any_value');
            expect(invokeSpy).toHaveBeenCalledWith('any_value');
            expect(result).toEqual({ data: '1' });
        });

        test('throws Not Found Exception when no data is found', async () => {
            const invokeSpy = jest.spyOn(sampleRepositoryReader, 'getById').mockResolvedValue(null);
            try {
                await getSample.invoke('any_value');
            }
            catch (error) {
                expect(invokeSpy).toHaveBeenCalledWith('any_value');
                expect(error).toBeInstanceOf(NotFoundException);
                expect(error.message).toEqual('Client id any_value not found')
            }
        });
    });
});
