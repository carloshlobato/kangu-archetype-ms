import { HttpStatus } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { SampleRequestClient } from '@src/infrastructure';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';
import { SampleOutboundService } from '@src/interface';

describe('interface :: outbound :: SampleOutboundService', () => {

    let sampleOutboundService: SampleOutboundService;
    let sampleRequestClient: any;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [SampleOutboundService],
            providers: [
                KanguLoggerService,
                {
                    provide: SampleRequestClient,
                    useValue: {
                        get: jest.fn()
                    }
                }
            ],
        }).setLogger(new KanguLoggerService).compile();

        sampleRequestClient = moduleRef.get<SampleRequestClient>(SampleRequestClient);
        sampleOutboundService = moduleRef.get<SampleOutboundService>(SampleOutboundService);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('getById()', () => {
        test('return data when no error occurs', async () => {
            jest.spyOn(sampleRequestClient, 'get').mockResolvedValueOnce({ data: 'data' });
            const promise = await sampleOutboundService.getById('any_value');

            expect(promise).toEqual('data');
        });

        test('return null when an error is thrown with status not found', async () => {
            jest.spyOn(sampleRequestClient, 'get').mockRejectedValue({ status: HttpStatus.NOT_FOUND });

            try {
                await sampleOutboundService.getById('any_value');
            } catch (error) {
                expect(error).toEqual(null);

            }
        });

        test('return null when an error is thrown with status not found', async () => {
            jest.spyOn(sampleRequestClient, 'get').mockRejectedValue(new Error());

            try {
                await sampleOutboundService.getById('any_value');
            } catch (error) {
                expect(error).toBeInstanceOf(Error);
            }
        });
    });
});
