import { maskCpf, maskSensitiveInformations } from '@src/utils/Masks';

describe('Utils :: Masks', () => {
    test('mask cpf value', () => {
        const maskedCpf = maskCpf("03024099999")
        expect(maskedCpf).toEqual('***.240.999-**');
    });

    test('return null when empty string is sent', () => {
        const maskedCpf = maskCpf("")
        expect(maskedCpf).toEqual(null);
    });

    test('return masked cpf obj', () => {
        const maskedCpf = maskSensitiveInformations({ cpf: "03024099999" })
        expect(maskedCpf).toEqual("{\"cpf\":\"***.240.999-**\"}");
    });

    test('return not masked obj when length is not 11', () => {
        const maskedCpf = maskSensitiveInformations({ cpf: "0302409999" })
        expect(maskedCpf).toEqual("{\"cpf\":\"0302409999\"}");
    });

    test('return not masked obj when length is not 11', () => {
        const maskedCpf = maskSensitiveInformations({ cpf: null })
        expect(maskedCpf).toEqual("{\"cpf\":null}");
    });

})