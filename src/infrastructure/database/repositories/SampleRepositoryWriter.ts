import { Inject, Injectable } from '@nestjs/common';
import { Collection, MongoClient } from 'mongodb';

import { ISampleRepositoryWriter, SampleEntity } from '@domain/index';

@Injectable()
export class SampleRepositoryWriter implements ISampleRepositoryWriter {
    private readonly collection: Collection;

    constructor(
        // @Inject('DATABASE_CONNECTION')
        // private dbConnection: MongoClient,
    ) {
        // this.collection = this.dbConnection.db().collection('sample');
    }

    // public async update(sampleEntity: SampleEntity) {
    // const filterBy = { sample_name: sampleEntity.name };
    // await this.collection.updateOne(
    //     filterBy,
    //     this.updateOperation({
    //         update_date: new Date().toISOString(),
    //         name: sampleEntity.name
    //     })
    // );

    // }

    public async delete(sampleEntity: SampleEntity) {
        return sampleEntity.name == 'teste' ? true : false;
    }

    // private updateOperation(options) {
    //     return { $set: options };
    // }
}

