export interface ISampleRepositoryReader {
    getById: (name: string) => Promise<any>;

}
