import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { ISampleRepositoryReader, ISampleRepositoryWriter, SampleEntity } from '@src/domain';

@Injectable()
export class DeleteSample {
    constructor(
        @Inject('SampleRepositoryReader')
        private readonly sampleRepositoryReader: ISampleRepositoryReader,
        @Inject('SampleRepositoryWriter')
        private readonly sampleRepositoryWriter: ISampleRepositoryWriter,
    ) { }

    public async invoke(id: string): Promise<SampleEntity> {
        const result = await this.sampleRepositoryReader.getById(id);

        if (!result) {
            throw new NotFoundException(`Client id ${id} not found`);
        }

        // this.sampleRepositoryWriter.delete()



        return result;
    }
}