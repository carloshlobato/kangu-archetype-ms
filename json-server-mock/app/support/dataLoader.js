import glob from 'glob';
import { resolve } from 'path';
import { pathToFileURL } from 'url';

import { getFileName } from './utils.js';

export const load = () => {
    const object = {};

    glob.sync('./data/*').forEach(file => {
        const [folderName] = file.split('/').slice(-1);
        object[folderName] = [];
    });

    Object.keys(object).forEach(key => {
        glob.sync(`./data/${key}/**/*.js`).forEach(async file => {
            const data = (await import(
                pathToFileURL(resolve(file))
            )).default;
            const filename = getFileName(file);

            object[key].push({
                filename,
                data,
            });
        });
    });

    global.mockData = object;
};
