import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength } from 'class-validator';

export class GetSampleDTO {
    @IsString()
    @ApiProperty({
        example: 'name',
        description: 'Value Name',
    })
    @MaxLength(100)
    name: string;

}
