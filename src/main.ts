import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { NestFactory } from '@nestjs/core';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';

import { MainModule } from './MainModule';
import { KanguLoggerService } from './infrastructure/logger/KanguLoggerService';
import {
  LoggerMiddleware,
  TraceIdMiddleware,
  RestrictRequestMiddleware,
} from './interface/http/inbound/middlewares';
import { AllExceptionsFilterToRFC7807 } from './interface/http/inbound/filters/AllExceptionsFilter';
import * as actuator from 'express-actuator';
const PORT = +process.env.PRE_PORT || 3000;
const MS_PREFIX = process.env.MS_PREFIX || '';

(async () => {
  const app = await NestFactory.create(
    MainModule,
    (process.env.NODE_ENV === 'local' ? {} : {
      logger: false
    })
  );

  const kanguLoggerService = app.get(KanguLoggerService);

  app.setGlobalPrefix(MS_PREFIX);
  app.use(helmet());
  app.use(actuator());
  app.use(TraceIdMiddleware);
  app.useLogger(kanguLoggerService);
  app.use(LoggerMiddleware(kanguLoggerService));
  app.use(RestrictRequestMiddleware(kanguLoggerService));
  app.useGlobalPipes(new ValidationPipe({
    exceptionFactory: (errors) => new BadRequestException(errors),
    validationError: {
      target: false,
      value: false
    }
  }));

  app.useGlobalFilters(new AllExceptionsFilterToRFC7807());

  const options = new DocumentBuilder()
    .setTitle('sample-archetype-nestjs')
    .setDescription('Archetype Nest JS Kangu')
    .setVersion('1.0.0')
    .addTag('samples')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup(`${MS_PREFIX}/v1/api-docs`, app, document);

  await app.listen(PORT);

  kanguLoggerService.kanguInfo(`Server running at PORT: ${PORT}`);
})();
