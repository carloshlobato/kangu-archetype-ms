export interface ISampleOutboundService {
    getById(name: string): Promise<any>
}