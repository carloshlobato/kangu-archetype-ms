import {
  getById,
} from '../support/genericHandlers.js';

const transactions = [
  {
    method: 'GET',
    path: '/sampleRest/:id',
    handler: getById({
      folder: 'sample',
      param: 'id'
    })
  },
];


export const customRoutes = [
  ...transactions,
];
