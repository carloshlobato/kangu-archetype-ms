import { Test } from '@nestjs/testing';
import { NotFoundException } from '@nestjs/common';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';
import { SampleService } from '@src/domain';

describe('domain :: services :: SampleService', () => {
    let sampleService: SampleService;
    let sampleRepositoryReader: any

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [SampleService],
            providers: [
                KanguLoggerService,
                {
                    provide: 'SampleRepositoryReader',
                    useValue: {
                        getById: jest.fn()
                    }
                },
                {
                    provide: 'KanguLoggerService',
                    useValue: {
                        error: jest.fn((data) => console.error(data))
                    }
                },
                {
                    provide: 'SampleRepositoryWriter',
                    useValue: {}
                }
            ],
        }).setLogger(new KanguLoggerService).compile();

        sampleService = moduleRef.get<SampleService>(SampleService);
        sampleRepositoryReader = moduleRef.get<'SampleRepositoryReader'>('SampleRepositoryReader');
    });

    describe('invoke()', () => {
        test('execute successfully when data is received and no error occurs', async () => {
            const invokeSpy = jest.spyOn(sampleRepositoryReader, 'getById').mockResolvedValue({ id: 'sample_id', name: 'sample_name' });
            const result = await sampleService.getSampleValues();
            expect(invokeSpy).toHaveBeenCalledWith('id');
            expect(result).toEqual({ id: 'sample_id', name: 'sample_name' });
        });

        test('return null when no info is retrieved from database', async () => {
            const invokeSpy = jest.spyOn(sampleRepositoryReader, 'getById').mockResolvedValue(null);
            const result = await sampleService.getSampleValues();
            expect(invokeSpy).toHaveBeenCalledWith('id');
            expect(result).toEqual(null);
        });
    });
});
