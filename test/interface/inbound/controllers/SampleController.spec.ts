import { Test } from '@nestjs/testing';
import { SampleController } from '@src/interface';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';
import { CreateSampleDTO } from '@src/interface/http/inbound/dto/CreateSampleDTO';

const invoke = jest.fn();
const createSampleDTO = new CreateSampleDTO();

describe('interface :: http :: inbound :: controllers :: SampleController', () => {
    let sampleController: SampleController;
    let getSample: any;
    let deleteSample: any;
    let kanguLoggerService: any;
    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [SampleController],
            providers: [
                KanguLoggerService,
                {
                    provide: 'GetSample',
                    useValue: {
                        invoke
                    }
                },
                {
                    provide: 'DeleteSample',
                    useValue: {
                        invoke: jest.fn()
                    }
                },
                {
                    provide: 'KanguLoggerService',
                    useValue: {
                        kanguError: jest.fn((data) => console.error(data))
                    }
                }
            ],
        }).setLogger(new KanguLoggerService).compile();

        getSample = moduleRef.get<'GetSample'>('GetSample');
        deleteSample = moduleRef.get<'DeleteSample'>('DeleteSample');
        kanguLoggerService = moduleRef.get<'KanguLoggerService'>('KanguLoggerService');
        sampleController = moduleRef.get<SampleController>(SampleController);
    });

    describe('delete()', () => {
        test('should execute successfully when non occurs any error', async () => {
            const invokeSpy = jest.spyOn(deleteSample, 'invoke').mockResolvedValue(() => { return {} });
            await sampleController.delete('any_value');
            expect(invokeSpy).toHaveBeenCalledWith('any_value');
        });

        test('throw error', async () => {
            const invokeSpy = jest.spyOn(deleteSample, 'invoke').mockRejectedValueOnce({ data: 'error' });
            const invokeErrorLogger = jest.spyOn(kanguLoggerService, 'kanguError').mockImplementationOnce((data) => console.error(data))
            try {
                await sampleController.delete('any_value');
            } catch (error) {
                expect(invokeSpy).toHaveBeenCalledWith('any_value');
                expect(error).toBeInstanceOf(Object);
                expect(invokeErrorLogger).toHaveBeenCalledTimes(1);
            }
        });
    });

    describe('create()', () => {
        test('should execute successfully when non occurs any error', async () => {
            const invokeSpy = jest.spyOn(deleteSample, 'invoke').mockResolvedValue(() => { return {} });
            createSampleDTO.name = 'any_name';
            await sampleController.create(createSampleDTO);
            expect(invokeSpy).toHaveBeenCalledWith('any_name');
        });

        test('throw error', async () => {
            const invokeSpy = jest.spyOn(deleteSample, 'invoke').mockRejectedValueOnce(new Error('error'));
            const invokeErrorLogger = jest.spyOn(kanguLoggerService, 'kanguError').mockImplementationOnce((data) => console.error(data))
            try {
                await sampleController.create(createSampleDTO);
            } catch (error) {
                expect(invokeSpy).toHaveBeenCalledWith(createSampleDTO.name);
                expect(error).toBeInstanceOf(Error);
                expect(invokeErrorLogger).toHaveBeenCalledTimes(1);
            }
        });
    });

    describe('getById()', () => {
        test('should execute successfully when non occurs any error', async () => {
            const invokeSpy = jest.spyOn(getSample, 'invoke').mockResolvedValue(null);
            await sampleController.getById('sample_id');
            expect(invokeSpy).toHaveBeenCalledWith('sample_id');
        });
        test('throw error', async () => {
            const invokeSpy = jest.spyOn(getSample, 'invoke').mockRejectedValueOnce(new Error('error'));
            const invokeErrorLogger = jest.spyOn(kanguLoggerService, 'kanguError').mockImplementationOnce((data) => console.error(data))
            try {
                await sampleController.getById('any_value');
            } catch (error) {
                expect(invokeSpy).toHaveBeenCalledWith('any_value');
                expect(error).toBeInstanceOf(Error);
                expect(invokeErrorLogger).toHaveBeenCalledTimes(1);
            }
        });
    });
});
