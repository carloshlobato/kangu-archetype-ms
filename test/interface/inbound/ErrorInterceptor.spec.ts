import { BadRequestException, HttpException, HttpStatus, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { ErrorInterceptor } from '@interface/index';
import { Observable } from 'rxjs';

describe('interface :: http :: inbound :: interceptors :: ErrorInterceptor', () => {
    let stubs;
    let errorInterceptor;

    beforeEach(() => {
        stubs = {
            context: {},
            next: {
                handle: () => new Observable(),
            },
        };

        errorInterceptor = new ErrorInterceptor();
    });

    test('Should return Unprocessable Entity error', done => {
        errorInterceptor = new ErrorInterceptor();

        stubs.next.handle = () =>
            new Observable(() => {
                throw new UnprocessableEntityException('teste');
            });

        errorInterceptor.intercept(stubs.context, stubs.next);
        stubs.next.handle().subscribe({
            error: error => {
                expect(error).toBeInstanceOf(UnprocessableEntityException);
                done();
            },
        });
    });

    test('Should process Business error', () => {
        const error = new UnprocessableEntityException('Unprocessable Entity');
        try {
            ErrorInterceptor.processError(error);
        } catch (e) {
            expect(e).toBeInstanceOf(HttpException);
            expect(e.status).toEqual(HttpStatus.UNPROCESSABLE_ENTITY);
            expect(e.message).toEqual('Unprocessable Entity');
        }
    });

    test('Should process Bad request', () => {
        const error = new BadRequestException('Bad Request');
        try {
            ErrorInterceptor.processError(error);
        } catch (e) {
            expect(e).toBeInstanceOf(HttpException);
            expect(e.status).toEqual(HttpStatus.BAD_REQUEST);
            expect(e.message).toEqual('Bad Request');
        }
    });

    test('Should process Not Found', () => {
        const error = new NotFoundException('Not Found');
        try {
            ErrorInterceptor.processError(error);
        } catch (e) {
            expect(e).toBeInstanceOf(HttpException);
            expect(e.status).toEqual(HttpStatus.NOT_FOUND);
            expect(e.message).toEqual('Not Found');
        }
    });

    test('Should process not Business error', () => {
        const error = new Error('Internal Server Error');
        try {
            ErrorInterceptor.processError(error);
        } catch (e) {
            expect(e).toBeInstanceOf(HttpException);
            expect(e.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
            expect(e.message).toEqual('Internal Server Error');
        }
    });
});
