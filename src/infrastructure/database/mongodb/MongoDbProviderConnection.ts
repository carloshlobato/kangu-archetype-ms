import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongoClient } from 'mongodb';

@Injectable()
export class MongoDbProviderConnection {
  private readonly config: any;
  private connection: MongoClient;

  constructor(configService: ConfigService) {
    this.connection = null;
    this.config = configService.get('mongoDb');
  }

  public async connect(): Promise<MongoClient> {
    if (this.connection)
      return this.connection;

    try {
      this.connection = await MongoClient.connect(
        this.buildMongoUrl(),
        this.buildMongoOptions(),
      );
      this.connection.readPreference.mode = 'nearest';
      this.setEventListeners();

      return this.connection;
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  private setEventListeners() {
    this.connection.on('connected', () =>
      console.log('Mongodb connection stablished'),
    );
    this.connection.on('disconnected', () =>
      console.log('Mongodb connection lost'),
    );
  }

  private buildMongoOptions() {
    const connectionOptions = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
    const { authSource, replicaSet = '' } = process.env;
    return {
      ...connectionOptions,
      authSource,
      replicaSet,
    };
  }

  private buildMongoUrl() {
    const {
      userName,
      password,
      port,
      hosts,
      dialect,
      replicaSet,
      authSource,
      database,
    } = this.config;
    const userPass = userName && password ? `${encodeURIComponent(userName)}:${encodeURIComponent(password)}@` : null;
    const parsedHosts = JSON.parse(hosts);
    const url = parsedHosts.reduce((prev, cur, i) => prev + cur + `:${port}${i === parsedHosts.length - 1 ? '' : ','}`, `${dialect}://${userPass}`);
    return `${url}/${database}?authSource=${authSource}${replicaSet ? `&replicaSet=${replicaSet}` : ''}`;
  }
}
