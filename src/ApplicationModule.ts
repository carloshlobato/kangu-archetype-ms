import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';

import {
  GetSample,
  DeleteSample
} from '@application/index';
import { InfrastructureModule } from './InfrastructureModule';
import { InterfaceOutboundModule } from './InterfaceOutboundModule';
import { KanguLoggerService } from './infrastructure/logger/KanguLoggerService';

const getSample = {
  provide: 'GetSample',
  useClass: GetSample
};



const deleteSample = {
  provide: 'DeleteSample',
  useClass: DeleteSample
}

const kanguLoggerService = {
  provide: 'KanguLoggerService',
  useClass: KanguLoggerService
}

const providers = [
  getSample,
  deleteSample,
  kanguLoggerService,
  TerminusModule
];


@Module({
  imports: [
    InfrastructureModule,
    InterfaceOutboundModule,
    TerminusModule
  ],
  controllers: [],
  providers,
  exports: providers,
})
export class ApplicationModule { }
