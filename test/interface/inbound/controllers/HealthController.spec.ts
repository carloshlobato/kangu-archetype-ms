import { Test } from '@nestjs/testing';
import { HealthController } from '@src/interface';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

describe('interface :: http :: inbound :: controllers :: HealthController', () => {
    let healthController: HealthController;
    let kanguLoggerService: KanguLoggerService;
    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [HealthController],
            providers: [
                {
                    provide: KanguLoggerService,
                    useValue: {
                        kanguError: jest.fn((data) => console.error(data)),
                        kanguInfo: jest.fn()
                    }
                }
            ],
        }).setLogger(new KanguLoggerService).compile();

        kanguLoggerService = moduleRef.get<KanguLoggerService>(KanguLoggerService);
        healthController = moduleRef.get<HealthController>(HealthController);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('healthcheck()', () => {
        test('should execute successfully when non occurs any error', async () => {
            const invokeSpy = jest.spyOn(kanguLoggerService, 'kanguInfo').mockImplementationOnce(() => { return true });
            const healthCheckResponse = await healthController.healthCheck();
            expect(invokeSpy).toHaveBeenCalledTimes(1);
            expect(healthCheckResponse).toEqual({
                status: 'up',
                message: 'servico ativo e conectado ao banco de dados',
            })
        });
        test('throw error', async () => {
            const invokeSpy = jest.spyOn(kanguLoggerService, 'kanguInfo').mockImplementationOnce(() => { throw new Error('error') });
            const invokeErrorSpy = jest.spyOn(kanguLoggerService, 'kanguError').mockImplementationOnce(() => { })
            try {
                await healthController.healthCheck();
            } catch (error) {
                expect(invokeSpy).toHaveBeenCalledTimes(1);
                expect(invokeErrorSpy).toHaveBeenCalledTimes(1);
            }
        });

    });
});
