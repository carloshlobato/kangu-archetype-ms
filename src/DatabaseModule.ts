import { Module } from '@nestjs/common';
import { MongoClient } from 'mongodb';

import {
  SampleRepositoryReader,
  MongoDbProviderConnection,
  SampleRepositoryWriter
} from './infrastructure';
import { KanguLoggerService } from './infrastructure/logger/KanguLoggerService';

const sampleRepositoryWriter = {
  provide: 'SampleRepositoryWriter',
  useClass: SampleRepositoryWriter,
};

const sampleRepositoryReader = {
  provide: 'SampleRepositoryReader',
  useClass: SampleRepositoryReader,
};

const mongoProvider = {
  provide: 'DATABASE_CONNECTION',
  useFactory: async (
    logger: KanguLoggerService,
    providerDbConnection: MongoDbProviderConnection,
    // ): Promise<Connection | MongoClient> => {
  ): Promise<any> => {
    try {
      // return await providerDbConnection.connect();
    } catch (err) {
      logger.kanguError(err);
      throw err;
    }
  },
  inject: [KanguLoggerService, MongoDbProviderConnection],
  exports: ['DATABASE_CONNECTION'],
};

const providers = [
  mongoProvider,
  sampleRepositoryWriter,
  sampleRepositoryReader,
  MongoDbProviderConnection,
  KanguLoggerService
];

@Module({
  imports: [],
  controllers: [],
  exports: providers,
  providers,
})
export class DatabaseModule { }
