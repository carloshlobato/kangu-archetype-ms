import { routes } from './routes.js'

export const setupMiddlewares = (jsonServer, server) => {
    server.use(jsonServer.bodyParser);
    server.use(jsonServer.rewriter(routes));
    server.use(jsonServer.defaults());
};
