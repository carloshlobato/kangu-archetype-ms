/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { ConsoleLoggerOptions, Injectable, LogLevel } from '@nestjs/common';
import { ICustomLogger } from './protocols/ICustomLogger';

@Injectable()
export class KanguLoggerService implements ICustomLogger {
    protected context?: string;
    protected options: ConsoleLoggerOptions;

    // tslint:disable-next-line: no-empty
    log(message: any, context?: string): any { }
    // tslint:disable-next-line: no-empty
    debug(message: any, context?: string): any { }
    // tslint:disable-next-line: no-empty
    error(message: any, trace?: string): any { }
    // tslint:disable-next-line: no-empty
    warn(message: any, context?: string): any { }

    kanguLog(level: string, message: string, trackId?: string): any {
        console.log(level, message, { trackId });
    }

    kanguError(message: string, trackId?: string): any {
        console.error(message, {
            trackId,
            dateTime: new Date(Date.now()).toLocaleString()

        });
    }

    kanguWarn(message: string, trackId?: string): any {
        console.warn(message, {
            trackId,
        });
    }

    kanguInfo(message: string, trackId?: string): any {
        console.info(message, {
            trackId,
            dateTime: new Date(Date.now()).toLocaleString()
        });
    }

    kanguDebug(message: string, trackId?: string): any {
        console.debug(message, {
            trackId,
        });
    }
}
