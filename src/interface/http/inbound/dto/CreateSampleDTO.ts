import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, MaxLength } from 'class-validator';

export class CreateSampleDTO {
    @IsString()
    @ApiProperty({
        example: 'name',
        description: 'Value Name',
    })
    @MaxLength(100)
    name: string;

    @IsString()
    @ApiProperty({
        required: false,
        example: 'email',
        description: 'Value Sample',
    })
    @MaxLength(100)
    @IsOptional()
    value: string;

}
