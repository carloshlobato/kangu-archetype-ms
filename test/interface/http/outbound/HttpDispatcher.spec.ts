import { Test } from '@nestjs/testing';
import { HttpDispatcher } from '@interface/http/outbound/HttpDispatcher';
import { HttpClient } from '@infrastructure/http/HttpClient';
import { of } from 'rxjs';
import { HttpStatus, InternalServerErrorException, RequestTimeoutException } from '@nestjs/common';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

const mockDispatch = () => ({
    toPromise: jest.fn(),
});

describe('infrastructure :: http :: clients :: HttpDispatcher', () => {
    let httpDispatcher: HttpDispatcher;
    let httpClient: HttpClient;
    let logger: KanguLoggerService;

    const input = {
        method: 'get',
        url: '/test',
        data: { test: true },
    };
    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [HttpDispatcher],
            providers: [
                KanguLoggerService,
                {
                    provide: HttpClient,
                    useValue: {
                        retryableRequest: mockDispatch
                    }
                }

            ],
        }).setLogger(new KanguLoggerService).compile();

        httpClient = moduleRef.get<HttpClient>(HttpClient);
        logger = moduleRef.get<KanguLoggerService>(KanguLoggerService);
        httpDispatcher = moduleRef.get<HttpDispatcher>(HttpDispatcher);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('dispatch()', () => {
        test('should execute successfully when non occurs any error', async () => {
            const response: any = {
                toPromise: jest.fn().mockImplementation(() => {
                    return 'returned_value'
                })
            }

            jest.spyOn(httpClient, 'retryableRequest').mockImplementation(() => of(response))

            const result = await httpDispatcher.dispatch(input);

            expect(result).toMatchObject(response);
        });
    });
});
