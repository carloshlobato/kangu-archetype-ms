import { Module } from '@nestjs/common';
import { ApplicationModule } from './ApplicationModule';
import { InfrastructureModule } from './InfrastructureModule';

import {
  HealthController,
  SampleController,
  ErrorInterceptor
} from '@interface/index';

@Module({
  imports: [
    InfrastructureModule,
    ApplicationModule,
  ],
  controllers: [
    SampleController,
    HealthController
  ],
  providers: [
    SampleController,
    HealthController,
    ErrorInterceptor,
  ],
})
export class InterfaceInboundModule { }
