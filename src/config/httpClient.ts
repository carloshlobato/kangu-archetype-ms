export default () => {
  const {
    PATH_URL,
    URL,
    PRE_REQUEST_TIMEOUT
  } = process.env;

  return {
    httpClient: {
      timeout: PRE_REQUEST_TIMEOUT,
      sampleServiceMs: {
        url: {
          base: URL,
          path: PATH_URL,
        }
      },
    },
  };
};
