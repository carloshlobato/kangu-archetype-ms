import { config } from 'dotenv';
import { Inject, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { HealthController } from '../http/inbound';
config();

@Injectable()
export class HealthCheckScheduler {
    constructor(
        @Inject('HealthController')
        private readonly healthCheckController: HealthController,
    ) { }

    @Cron(`*/10 * * * * *`)
    public async schedule() {
        this.healthCheckController.healthCheck();
    }

}
