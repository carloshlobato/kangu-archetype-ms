import { Test } from '@nestjs/testing';
import { SampleRepositoryReader } from '@src/infrastructure';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

describe('infrastructure :: database :: repositories :: SampleRepositoryReader', () => {
    let sampleRepositoryReader: SampleRepositoryReader;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [SampleRepositoryReader],
            providers: [],
        }).setLogger(new KanguLoggerService).compile();

        sampleRepositoryReader = moduleRef.get<SampleRepositoryReader>(SampleRepositoryReader);
    });

    describe('getById()', () => {
        test('execute successfully when data is received and no error occurs', async () => {
            const result = await sampleRepositoryReader.getById('teste');
            expect(result).toEqual({ id: 'sample_id', name: 'sample_name' });
        });

        test('returns null when no data is found', async () => {
            const result = await sampleRepositoryReader.getById('any_value');
            expect(result).toEqual(null);
        });
    });
});
