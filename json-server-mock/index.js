import { load as dataLoader } from './app/support/dataLoader.js';
import { start as startApplication } from './app/server.js';

startApplication(dataLoader);
