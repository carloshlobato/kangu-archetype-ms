import { Request, Response, NextFunction } from 'express';
import { v4 as uuidV4 } from 'uuid';

export function TraceIdMiddleware(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  req.__trackId__ = req.__trackId__ || uuidV4();
  next();
}
