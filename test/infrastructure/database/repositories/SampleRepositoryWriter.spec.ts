import { Test } from '@nestjs/testing';
import { SampleEntity } from '@src/domain';
import { SampleRepositoryReader, SampleRepositoryWriter } from '@src/infrastructure';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

describe('infrastructure :: database :: repositories :: SampleRepositoryWriter', () => {
    let sampleRepositoryWriter: SampleRepositoryWriter;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [SampleRepositoryWriter],
            providers: [],
        }).setLogger(new KanguLoggerService).compile();

        sampleRepositoryWriter = moduleRef.get<SampleRepositoryWriter>(SampleRepositoryWriter);
    });

    describe('delete()', () => {
        test('returns true when sample entity name is teste', async () => {
            const newSampleEntity = new SampleEntity();
            newSampleEntity.name = 'teste'
            const result = await sampleRepositoryWriter.delete(newSampleEntity);
            expect(result).toEqual(true);
        });

        test('returns false when sample entity name is not teste', async () => {
            const newSampleEntity = new SampleEntity();
            newSampleEntity.name = 'any_value'
            const result = await sampleRepositoryWriter.delete(newSampleEntity);
            expect(result).toEqual(false);
        });
    });
});
