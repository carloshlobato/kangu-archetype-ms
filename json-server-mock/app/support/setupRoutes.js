import { customRoutes } from '../routes/customRoutes.js';

export const setupRoutes = (server, router) => {
    customRoutes.forEach(({
        method,
        path,
        handler
    }) => {
        server[method.toLowerCase()](path, handler);
    });

    server.use(router);
};
