import { Test } from '@nestjs/testing';
import { KanguLoggerService } from '@src/infrastructure/logger/KanguLoggerService';

import { HealthCheckScheduler, HealthController } from '@src/interface';

describe('interface :: schedulers :: HealthCheckScheduler', () => {
    let healthController: any;
    let healthCheckScheduler: HealthCheckScheduler;
    beforeEach(async () => {

        const moduleRef = await Test.createTestingModule({
            controllers: [HealthCheckScheduler],
            providers: [
                {
                    provide: 'HealthController',
                    useValue: {
                        healthCheck: jest.fn()
                    }
                }
            ],
        }).setLogger(new KanguLoggerService).compile();

        healthCheckScheduler = moduleRef.get<HealthCheckScheduler>(HealthCheckScheduler);
        healthController = moduleRef.get<'HealthController'>('HealthController');
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('schedule()', () => {
        test('invoke is called', async () => {

            const spyHealthCheck = jest.spyOn(healthController, 'healthCheck');

            await healthCheckScheduler.schedule();

            expect(spyHealthCheck).toHaveBeenCalledTimes(1);

        });
    });
});
