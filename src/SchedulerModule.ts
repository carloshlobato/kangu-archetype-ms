import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { ApplicationModule } from './ApplicationModule';
import { InfrastructureModule } from './InfrastructureModule';
import { HealthCheckScheduler } from './interface';
import { InterfaceOutboundModule } from './InterfaceOutboundModule';
import {
  HealthController,
} from '@interface/index';
import { KanguLoggerService } from './infrastructure/logger/KanguLoggerService';

const healthController = {
  provide: 'HealthController',
  useClass: HealthController
};

const providers = [
  HealthCheckScheduler,
  KanguLoggerService,
  healthController
];

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ApplicationModule,
    InterfaceOutboundModule,
    InfrastructureModule
  ],
  providers,
  exports: providers,
})
export class SchedulerModule { }
