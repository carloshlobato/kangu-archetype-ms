export const maskCpf = (cpf: string) => cpf && cpf.length === 11 ? `${cpf.slice(0, 3).replace(/^\d+$/g, '*'.repeat(3))}.${cpf.slice(3, 6)}.${cpf.slice(6, 9)}-${cpf.slice(9, 11).replace(/^\d+$/g, '*'.repeat(2))}` : null;

export const maskSensitiveInformations = (obj: any) => JSON.stringify(
  obj, (key, value) => {
    if (key === 'cpf' && value?.length === 11) {
      return maskCpf(value);
    }
    return value;
  }
);