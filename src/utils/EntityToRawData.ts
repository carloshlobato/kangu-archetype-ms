export const entityToRawData = (value: any): any => {
  return JSON.parse(JSON.stringify(value));
};
